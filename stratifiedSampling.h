#ifndef STRATIFIEDSAMPLING_H
#define STRATIFIEDSAMPLING_H

#include "point.h"

#include <vector>

namespace Sampling{
    namespace Stratified{
        float random(float min = -1.0, float max = 1.0);

        void sampling(float min, float max, float sizeCell, int ptByCell, std::vector<Point> & points);
    }
}
#endif // STRATIFIEDSAMPLING_H
