#ifndef SAMPLER_H
#define SAMPLER_H

#include <vector>

typedef unsigned int uint;
typedef std::pair<float,float> Sample;

class Sampler
{
public:
    Sampler();
    const std::vector<Sample>& getSampling(){
        return _sampling;
    }
    const std::vector<std::pair<double,double>>& getCummulativeStarDiscrepancy(){
        return _cummulativeStarDiscrepancy;
    }

    const std::vector<Sample>& getJitteredSampling(){
        return _jitteredSampling;
    }
    const Sample& getSample(uint numSample){
        return _sampling[numSample];
    }
    const std::pair<double,double>& getLastJitter(){
        return _lastJitter;
    }
    const std::pair<double,double>& getDiscrepancy(){
        return _starDiscrepancy;
    }

    void generateHaltonSampling(uint nbPoints, uint dimensionX, uint dimensionY, uint offsetStartSequence=0);
    void generateHammersleySampling(uint nbPoints, uint offsetStartSequence=0);
    void generateWangSampling(uint nbPoints);
    void generateShaderToySampling(uint nbPoints);
    void generateRegularSampling(uint nbPoints);
    void generatePlainRandomSampling(uint nbPoints);
    void generateJitteredSampling(uint nbPoints);
    void generateSobolSampling(uint nbPoints, uint dimensionX, uint dimensionY, uint offsetStartSequence=0);
    void generatePoissonSampling(uint nbPoints, const double& radius);
    void generatePoissonSamplingJittered(uint nbPoints, const double& radius);
    void generatePoissonSamplingRotation(uint nbPoints, const double& r);
    void projectSamplingOnDisk();
    const std::vector<Sample>& applyJitter(const double& radius);
    const std::vector<Sample>& applyJitter(const double& jitterX, const double& jitterY);
    const std::vector<Sample>& applyRotation();
    void orderSampling();
    const std::pair<double,double>& computeStarDiscrepancy();
    void computeCummulativeStarDiscrepancy();

    static float wangHash(uint &seed);
    static float halton(uint index, uint base);
    static double drand48();

private:
    std::vector<Sample> _sampling;
    std::vector<Sample> _jitteredSampling;
    std::pair<double,double> _lastJitter;
    std::pair<double,double> _starDiscrepancy;
    std::vector<std::pair<double,double>> _cummulativeStarDiscrepancy;
};

#endif // SAMPLER_H
