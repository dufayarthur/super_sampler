#define _CRT_RAND_S//use by drand48

#include "sampler.h"

#include <cmath>
#include <algorithm>

#include "sobol.h"
#include "point.h"
#include "mortonCode.h"
#include "threaddartthrowing.h"
#include "star_discrepancy.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif


Sampler::Sampler()
{
}

void Sampler::generateHaltonSampling(uint nbPoints, uint dimensionX, uint dimensionY, uint offsetStartSequence)
{
    _sampling.clear();
    _sampling.resize( nbPoints );

    dimensionX = std::max(dimensionX,uint(2));
    dimensionY = std::max(dimensionY,uint(2));

    for (uint i = 0; i < nbPoints; i++)
    {
        _sampling[i] = std::make_pair(halton(i + offsetStartSequence, dimensionX),halton(i + offsetStartSequence, dimensionY));
    }
}

void Sampler::generateHammersleySampling(uint nbPoints, uint offsetStartSequence)
{
    _sampling.clear();
    _sampling.resize( nbPoints );

    for (uint i = 0; i < nbPoints; i++)
    {
        _sampling[i] = std::make_pair(static_cast<float>(i)/static_cast<float>(nbPoints),halton(i+offsetStartSequence,2));
    }
}

void Sampler::generateWangSampling(uint nbPoints)
{
    _sampling.clear();
    _sampling.resize( nbPoints );

    uint seed = rand();

    for (uint i = 0; i < nbPoints; i++)
    {
        _sampling[i] = std::make_pair(wangHash(seed),wangHash(seed));
    }
}

void Sampler::generateShaderToySampling(uint nbPoints)
{
    //pixel coordinates
    int pX = drand48()*1280;
    int pY = drand48()*720;
    //compute a seed for the rng based on pixel coordinates
    //void seed(int x, int y, int screenWidth)
    //{
    float seed = 0.0174532 * pY * 1280 + pX;
    seed *= sin(seed);
    //return seed;
    //}
    //vec2 rng(float seed)
    //{
    //fract returns the fractional part of x. It is calculated as
    //x - floor(x).

    _sampling.clear();
    _sampling.resize( nbPoints );

    for (uint i = 0; i < nbPoints; i++)
    {
        seed+=0.1;
        double x =  43758.5453123 * sin(seed);
        x = x - floor(x);

        seed+=0.1;
        double y =  22578.1459123 * sin(seed);
        y = y - floor(y);

        _sampling[i] = std::make_pair(static_cast<float>(x),static_cast<float>(y));


        //return fract(sin(vec2(seed+=0.1,seed+=0.1)) *
        //vec2(43758.5453123,22578.1459123));
    }
}

void Sampler::generateRegularSampling(uint nbPoints)
{
    _sampling.clear();

    uint nb = uint(sqrt(nbPoints));

    _sampling.resize( nb*nb );

    float offset = 0.5f / static_cast<float>(nb);

    for (uint i = 0; i < nb; i++)
    {
        for (uint j = 0; j < nb; j++)
        {
            _sampling[i*nb+j] = std::make_pair(offset+static_cast<float>(i) / static_cast<float>(nb),offset+static_cast<float>(j) / static_cast<float>(nb));
        }
    }
}

void Sampler::generatePlainRandomSampling(uint nbPoints)
{
    _sampling.clear();
    _sampling.resize( nbPoints );

    for (uint i = 0; i < nbPoints; i++)
    {
        _sampling[i] = std::make_pair(static_cast<float>(drand48()),static_cast<float>(drand48()));
    }
}

void Sampler::generateJitteredSampling(uint nbPoints)
{
    uint step = uint(sqrt(nbPoints));
    float stepX = 1.f/(float)step;
    float stepY = 1.f/(float)step;

    _sampling.clear();
    _sampling.resize( step * step );

    for (uint i = 0; i < step; i++)
    {
        for (uint j = 0; j < step; j++)
        {
            float tempX = (float)i*stepX + (float)drand48()*stepX;
            float tempY = (float)j*stepY + (float)drand48()*stepY;

            _sampling[ i*step + j] = std::make_pair(tempX, tempY);
         }
    }
}

void Sampler::generateSobolSampling(uint nbPoints, uint dimensionX, uint dimensionY, uint offsetStartSequence)
{
    _sampling.clear();
    _sampling.resize( nbPoints );

    double** tempPoints;
    uint maxDimension = std::max(dimensionX,dimensionY) + 1;
    tempPoints = sobol_points(nbPoints + offsetStartSequence, maxDimension);

    for (uint i = 0; i < nbPoints; i++)
    {
        _sampling[ i ] = std::make_pair(static_cast<float>(tempPoints[i+offsetStartSequence][dimensionX]), static_cast<float>(tempPoints[i+offsetStartSequence][dimensionY]));
    }

    for (uint i = 0; i < nbPoints + offsetStartSequence; i++)
    {
        delete [] tempPoints[i];
    }
    delete [] tempPoints;
}

void Sampler::generatePoissonSampling(uint nbPoints, const double& radius)
{
    /*TODO
     * use this for poisson sampling
     *
        QThread* thread = new QThread;
        ThreadDartThrowing* worker = new ThreadDartThrowing();
        worker->setData(&_sampling);
        worker->setParameters(unsigned int(nbPoints), ui->_dsbR->value());
        worker->moveToThread(thread);
        //connect(worker, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
        connect(worker, SIGNAL(progressValue(int)), this, SLOT(setProgressValue(int)));
        connect(thread, SIGNAL(started()), worker, SLOT(process()));
        connect(worker, SIGNAL(finished()), this, SLOT(refresh()));
        connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
        connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
        thread->start();
        */

    /*
     * int margin = ui->_spImageMargin->value();
    unsigned int M = ui->_sbImageSize->value();
    float x, y, x2, y2;
    int posX;
    int posY;

    QRgb pointColor = qRgb(ui->_sbColorR->value(),ui->_sbColorG->value(),ui->_sbColorB->value());

    ui->_progressBar->setValue(0);

    if( ! ui->_cbKeepLastSampling->isChecked() )
    {
        _im = QImage(M,M,QImage::Format_RGB888);
        _im.fill(QColor(255,255,255));

        _im2 = QImage(M,M,QImage::Format_RGB888);
        _im2.fill(QColor(255,255,255));
    }

    _imDimX = QImage(M,10,QImage::Format_RGB888);
    _imDimX.fill(QColor(255,255,255));

    _imDimY = QImage(M,10,QImage::Format_RGB888);
    _imDimY.fill(QColor(255,255,255));

    // poisson sampling
    float *xs;
    float *ys;
    //float radius = _im.width()/2-10;
    xs = new float[nbPoints];
    ys = new float[nbPoints];

    float xl, yl;
    bool tooClose;

    xs[0] = 100.0f * drand48();
    ys[0] = 100.0f * drand48();

    float maxX,maxY,minX,minY;
    maxX = -10000;
    maxY = -10000;
    minX = 10000;
    minY = 10000;

    float offsetX = 2.0*(drand48()-0.5)*r;
    float offsetY = 2.0*(drand48()-0.5)*r;

    for (int i = 0; i < nbPoints; i++)
    {
        do {
            //x = (float)im3.width() * drand48();
            //y = (float)im3.height() * drand48();

            float randR = drand48();
            float theta = 2.0*M_PI*drand48();
            x = randR*cos(theta);
            y = randR*sin(theta);

            tooClose = false;
            for (int j = 0; j < i; j++) {
                xl = x - xs[j];
                yl = y - ys[j];
                if (xl*xl + yl*yl < r*r) { tooClose = true; break; }
            }
        } while (tooClose == true);

        xs[i] = x;
        ys[i] = y;

        x2 = x + offsetX;
        y2 = y + offsetY;

        bool reprojected = false;

        double r2 = sqrt(x2*x2+y2*y2);
        //point outside of the disk
        if(r2>1.0)
        {
            double phi = acos(x2/r2);
            r2 = 2.0-r2;
            phi = phi+M_PI;

            x2 = r2*cos(phi);
            y2 = r2*sin(phi);
            reprojected = true;
        }

        //Draw initial poisson distribution
        posX = margin+(x+1.0)/2.0*(_im.width()-2*margin);
        posY = margin+(y+1.0)/2.0*(_im.height()-2*margin);

        drawPoint(_im,posX,posY,pointColor);
        drawPoint(_im2,posX,posY,pointColor);

        drawPoint(_imDimX,posX,5,qRgb(0,0,0));
        drawPoint(_imDimY,posY,5,qRgb(0,0,0));

        //Draw jitterred poisson distribution
        posX = margin+(x2+1.0)/2.0*(_im.width()-2*margin);
        posY = margin+(y2+1.0)/2.0*(_im.height()-2*margin);

        if(reprojected)
        {
            drawPoint(_im,posX,posY,qRgb(255,0,255));
        }
        else
        {
            drawPoint(_im,posX,posY,qRgb(0,0,255));
        }

        //Store min and max values of initial poissson distribution
        maxX = max(maxX,xs[i]);
        minX = min(minX,xs[i]);
        maxY = max(maxY,ys[i]);
        minY = min(minY,ys[i]);

        ui->_progressBar->setValue(static_cast<float>(i+1)/static_cast<float>(nbPoints));
    }

    delete [] xs;
    delete [] ys;


    //Draw circles around distributions
    QPainter painter;
    painter.begin(&_im);
    painter.setPen(Qt::black);
    painter.drawEllipse(margin,margin,_im.width()-2*margin,_im.height()-2*margin);
    painter.end();

    painter.begin(&_im2);
    painter.setPen(Qt::black);
    painter.drawEllipse(margin,margin,_im2.width()-2*margin,_im.height()-2*margin);

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    ui->_lImage2->setPixmap(QPixmap::fromImage(_im2));
    ui->_lImageDimX->setPixmap(QPixmap::fromImage(_imDimX));
    ui->_lImageDimY->setPixmap(QPixmap::fromImage(_imDimY));

    ui->_progressBar->setValue(100);
    */
}

void Sampler::generatePoissonSamplingJittered(uint nbPoints, const double& radius)
{
    /*
    QString info;
    unsigned int M = ui->_sbImageSize->value();

    if( ! ui->_cbKeepLastSampling->isChecked() )
    {
        _im = QImage(M,M,QImage::Format_RGB888);
        _im.fill(QColor(255,255,255));
    }

    float x, y, x2, y2;
    float oldx2,oldy2;
    QPoint pos;

    int margin = ui->_spImageMargin->value();

    int nbSampling = 512;
    PoissonSampling poissonSampling(nbSampling);

    int nbIter = float(nbPoints)/float(nbSampling);

    for (int i = 0; i < nbIter; i++)
    {
        //float offsetX = 2.0*(drand48()-0.5)*r;
        //float offsetY = 2.0*(drand48()-0.5)*r;
        float offsetTheta = 2.0*M_PI*drand48();
        float offsetR = drand48()*r;
        float offsetX = offsetR*cos(offsetTheta);
        float offsetY = offsetR*sin(offsetTheta);

        for(int j=0;j<nbSampling;j++)
        {
            x = poissonSampling.getSampleX(j);
            y = poissonSampling.getSampleY(j);

            x2 = x + offsetX;
            y2 = y + offsetY;

            bool reprojected = false;


            double r2 = sqrt(x2*x2+y2*y2);
            //point outside of the disk
            if(r2>1.0)
            {
               /*OLD method
                *  double phi = acos(x2/r2);
                r2 = 2.0-r2;
                phi = phi+M_PI;

                x2 = r2*cos(phi);
                y2 = r2*sin(phi);
                */
/*
                QPointF p(offsetX/2.0, offsetY/2.0);
                QPointF ortho = normalize(QPointF(-offsetY,offsetX));
                QPointF t = QPointF(p.x()-x2,p.y()-y2);

                //float length = sqrt(t.x()*t.x()+t.y()*t.y());
                //t = normalize(t);

                QPointF t2 = reflect(t,ortho);
                //t2 *= length;

                oldx2 = x2;
                oldy2 = y2;

                x2 = p.x()+t2.x();
                y2 = p.y()+t2.y();

                reprojected = true;

            }

            pos = normalizeLandmarkToImageLandmark(QPointF(x2,y2),margin,_im.width(),_im.height());

            QPainter painter;
            painter.begin(&_im);
            QPen pen;
            pen.setWidth(3);
            painter.setPen(pen);


            if(reprojected)
            {

                //pen.setColor(Qt::green);
                pen.setColor(Qt::black);
                painter.setPen(pen);

                painter.drawPoint(pos);

                pen.setColor(Qt::black);
                painter.setPen(pen);


                if(ui->_cbReprojectionLines->isChecked())
                {
                    QPen penLine;
                    penLine.setWidth(1);
                    penLine.setColor(Qt::blue);
                    painter.setPen(penLine);


                    QPoint start,end;
                    start = normalizeLandmarkToImageLandmark(QPointF(oldx2,oldy2),margin,_im.width(),_im.height());
                    end = normalizeLandmarkToImageLandmark(QPointF(x,y),margin,_im.width(),_im.height());

                    painter.drawLine(start,end);



                    start = normalizeLandmarkToImageLandmark(QPointF(x2,y2),margin,_im.width(),_im.height());

                    painter.drawLine(start,end);

                    float testAngle;
                    testAngle = dot( normalize(QPointF(oldx2,oldy2)-QPointF(x,y)), normalize(QPointF(x2,y2)-QPointF(x,y))  );
                    QString qs;
                    qs.setNum(testAngle);
                    info+=qs+"\n";

                    painter.setPen(pen);
                }

            }
            else
            {
                //pen.setColor(Qt::red);
                pen.setColor(Qt::black);
                 painter.setPen(pen);
                painter.drawPoint(pos);
            }

            pen.setWidth(3);
            painter.setPen(pen);

            if(i==0){
                pos = normalizeLandmarkToImageLandmark(QPointF(x,y),margin,_im.width(),_im.height());

                //pen.setColor(qRgb(255,0,255));
                pen.setColor(Qt::black);
                painter.setPen(pen);
                painter.drawPoint(pos);
            }
            painter.end();

        }


    }

    QPainter painter;
    painter.begin(&_im);
    painter.setPen(Qt::black);
    //painter.drawEllipse(im3.width()/2,im3.height()/2,im3.width()/2-10,im3.width()/2-10);
    painter.drawEllipse(margin,margin,_im.width()-2*margin,_im.height()-2*margin);
    painter.end();

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    ui->_lImage2->setText(info);*/
}

void Sampler::generatePoissonSamplingRotation(uint nbPoints, const double& r)
{
    /*TODO add this
    unsigned int M = ui->_sbImageSize->value();

    if( ! ui->_cbKeepLastSampling->isChecked() )
    {
        _im = QImage(M,M,QImage::Format_RGB888);
        _im.fill(QColor(255,255,255));
    }

    float x, y, x2, y2;
    int posX;
    int posY;

    int nbSampling = 8;
    PoissonSampling poissonSampling(nbSampling);

    int nbIter = float(nbPoints)/float(nbSampling);

    for (int i = 0; i < nbIter; i++)
    {
        double angle = drand48()*M_PI;

        double valMatrix[]={cos(angle), -sin(angle), 0.0,
                            sin(angle), cos(angle), 0.0,
                            0.0, 0.0, 1.0};

        QGenericMatrix<3,3,double> rotationMatrix(valMatrix);
        //rotationMatrix.fill(valMatrix);



        for(int j=0;j<nbSampling;j++)
        {
            x = poissonSampling.getSampleX(j);
            y = poissonSampling.getSampleY(j);

            double valVec[3]={x,y,0};
            QGenericMatrix<3,1,double> vec(valVec);

            vec = vec * rotationMatrix;

            x2 = vec.constData()[0];
            y2 = vec.constData()[1];

            posX = 5+(x2+1.0)/2.0*(_im.width()-10);
            posY = 5+(y2+1.0)/2.0*(_im.height()-10);

            drawPoint(_im,posX,posY);

            if(i==0){
                posX = 5+(x+1.0)/2.0*(_im.width()-10);
                posY = 5+(y+1.0)/2.0*(_im.height()-10);

                drawPoint(_im,posX,posY,qRgb(255,0,255));
            }

        }


    }

    QPainter painter;
    painter.begin(&_im);
    painter.setPen(Qt::black);
    //painter.drawEllipse(im3.width()/2,im3.height()/2,im3.width()/2-10,im3.width()/2-10);
    painter.drawEllipse(5,5,_im.width()-10,_im.height()-10);
    painter.end();

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    */
}

void Sampler::projectSamplingOnDisk()
{
    for (uint i = 0; i < _sampling.size(); i++)
    {
        float x = sqrt(_sampling[i].second) * cos(2.0*M_PI*_sampling[i].first);
        float y = sqrt(_sampling[i].second) * sin(2.0*M_PI*_sampling[i].first);

        //convert point from [-1,1]x[-1,1] to [0,1]x[0,1]
        _sampling[i].first = 0.5 * (x + 1.0);
        _sampling[i].second = 0.5 * (y + 1.0);
    }
}

const std::vector<Sample>& Sampler::applyJitter(const double &radius)
{
    double jitterX = (2.0*drand48()-1.0) * radius;
    double jitterY = (2.0*drand48()-1.0) * radius;

    return applyJitter(jitterX, jitterY);
}

const std::vector<Sample>& Sampler::applyJitter(const double &jitterX, const double &jitterY)
{
    _jitteredSampling.resize(_sampling.size());

    for (uint i = 0; i < _sampling.size(); i++)
    {
        float x = _sampling[i].first + jitterX;
        float y = _sampling[i].second + jitterY;

        if(x<0.0)
            x += 1.0;
        else if(x>=1.0)
            x -= 1.0;

        if(y<0.0)
            y += 1.0;
        else if(y>=1.0)
            y -= 1.0;

        _jitteredSampling[i] = std::make_pair(x,y);
    }
    _lastJitter = std::make_pair(jitterX,jitterY);
    return _jitteredSampling;
}

const std::vector<Sample>& Sampler::applyRotation()
{
    _jitteredSampling.resize(_sampling.size());

    float tempAngle = drand48()*2.0*M_PI;

    for (uint i = 0; i < _sampling.size(); i++)
    {
        float x = _sampling[i].first;
        float y = _sampling[i].second;


        //first get from cartesian coords to polar coords
        x = x*2.f-1.f;
        y = y*2.f-1.f;

        //float x = sqrt(_sampling[i].second) * cos(2.0*M_PI*_sampling[i].first);
        //float y = sqrt(_sampling[i].second) * sin(2.0*M_PI*_sampling[i].first);

        //convert point from [-1,1]x[-1,1] to [0,1]x[0,1]
        //_sampling[i].first = 0.5 * (x + 1.0);
        //_sampling[i].second = 0.5 * (y + 1.0);



        float ro = sqrt(x*x+y*y);

        float theta;
        if(abs(ro)>0.f)
        {
            if( y>=0.f)
                theta = acos(x/ro);
            else
                theta = -acos(x/ro);
        }
        else
            theta = 0.f;


        //apply rotation
        theta += tempAngle;//angle;

        //return to cartesian coords
        x = ro*cos(theta);
        y = ro*sin(theta);

        x = 0.5f*(x+1.f);
        y = 0.5f*(y+1.f);

        _jitteredSampling[i] = std::make_pair(x,y);
    }
    return _jitteredSampling;
}

void Sampler::orderSampling()
{
    std::vector<Point> points;

    points.resize(_sampling.size());

    for(uint i=0;i<_sampling.size();i++)
    {
        points[i] = Point(_sampling[i].first, _sampling[i].second);
    }

    std::vector<Coordinates2D> coords;
    MortonCode morton(points);
    morton.codeForCoordinates2D(coords);

    for(unsigned int i=0;i<_sampling.size();i++)
    {
        _sampling[i].first = coords[i].getX();
        _sampling[i].second = coords[i].getY();
    }
}

const std::pair<double, double> &Sampler::computeStarDiscrepancy()
{
    getStarDiscrepancy(_sampling,_sampling.size(),_starDiscrepancy,0.001);
    return _starDiscrepancy;
}

void Sampler::computeCummulativeStarDiscrepancy()
{
    _cummulativeStarDiscrepancy.resize(_sampling.size());
    for(unsigned int i=0;i<_sampling.size();i++)
    {
        std::pair<double,double> starD;
        getStarDiscrepancy(_sampling,i+1,starD,0.001);
        _cummulativeStarDiscrepancy[i] = starD;
    }
}

float Sampler::wangHash(uint &seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return (float(seed)) / 0xffffffffU;
}

float Sampler::halton(uint index, uint base)
{
    float res = 0.0;
    float f = 1.f / float(base);
    int i = index;
    while(i>0)
    {
      res = res + f * (i % base);
      i = int((float)i/(float)base);//int(floor(i/int(base);
      f = f / float(base);
    }
    return res;
}

double Sampler::drand48()
{
    unsigned int number;
    rand_s(&number);
    return (double)number/(double)UINT_MAX;
}


/*

float dot(const QPointF& a, const QPointF& b)
{
    return a.x()*b.x() + a.y()*b.y();
}

QPointF reflect(const QPointF& i, const QPointF& n)
{
    return  i - 2.0 * dot ( n , i ) * n;
}

QPointF normalize(const QPointF& v)
{
    float norm = sqrt(v.x()*v.x()+v.y()*v.y());
    if(norm>0.f)
    {
        return v/norm;
    }
    return v;
}
*/
