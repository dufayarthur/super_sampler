#include "applicationsettings.h"

ApplicationSettings* ApplicationSettings::_instance = nullptr;

ApplicationSettings::ApplicationSettings()
{
}

ApplicationSettings::~ApplicationSettings()
{
}
std::string ApplicationSettings::currentDirectory() const
{
    return _currentDirectory;
}

void ApplicationSettings::setCurrentDirectory(const std::string &currentDirectory)
{
    _currentDirectory = currentDirectory;
}


ApplicationSettings *ApplicationSettings::Instance()
{
    if(!_instance)
        _instance = new ApplicationSettings();

    return _instance;
}

void ApplicationSettings::kill()
{
    if(_instance)
    {
        delete _instance;
        _instance = nullptr;
    }
}
