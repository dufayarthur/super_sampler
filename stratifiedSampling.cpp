#include "stratifiedSampling.h"

#include <stdlib.h>
#include <math.h>

namespace Sampling{
    namespace Stratified{
        float random(float min, float max){
            float f = (float)rand() / (float)RAND_MAX;
            return min + f * (max - min);
        }

        void sampling(float min, float max, float sizeCell, int ptByCell, std::vector<Point> & points){
            float nbCells = (max - min) / (float)sizeCell;

            float d = (max - min)/nbCells;

            for(float i=min; i<max; i+=d){
                for(float j=min; j<max; j+=d){
                    for(int k=0; k<ptByCell; k++){
                        Point pt = Point(random(i, i+d), random(j, j+d));
                        if(sqrt(pt.getX()*pt.getX() + pt.getY()*pt.getY()) <= (max-min)/2.0)
                            points.push_back(pt);
                    }
                }
            }
        }
    }
}
