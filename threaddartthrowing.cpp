#include "threaddartthrowing.h"

#include <cmath>
#include <random>
#include <chrono>

using std::max;
using std::min;

#ifndef M_PI
#define M_PI 3.14159265359
#endif

ThreadDartThrowing::ThreadDartThrowing()
{
    _sampling = nullptr;
    _nbPoints = 0;
    _radius = 0;
}

ThreadDartThrowing::~ThreadDartThrowing()
{
}

void ThreadDartThrowing::setData(std::vector<std::pair<float,float>>* sampling)
{
    _sampling = sampling;
}

void ThreadDartThrowing::setParameters(unsigned int nbPoints, float radius)
{
    _nbPoints = nbPoints;
    _radius = radius;
}

// --- PROCESS ---
// Start processing data.
void ThreadDartThrowing::process()
{
    if( !_sampling || _nbPoints==0 )
        return;

    // allocate resources using new here
    qDebug("Hello World!");

    double r = _radius;

    float x, y, x2, y2;
    int posX;
    int posY;

    // poisson sampling
    float *xs;
    float *ys;
    //float radius = _im.width()/2-10;
    xs = new float[_nbPoints];
    ys = new float[_nbPoints];
    _sampling->resize(_nbPoints);

    float xl, yl;
    bool tooClose;

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 rng(seed);

    xs[0] = 100.0f * double(rng())/(rng.max());
    ys[0] = 100.0f * double(rng())/(rng.max());

    float maxX,maxY,minX,minY;
    maxX = -10000;
    maxY = -10000;
    minX = 10000;
    minY = 10000;

    float offsetX = 2.0*(double(rng())/(rng.max())-0.5)*r;
    float offsetY = 2.0*(double(rng())/(rng.max())-0.5)*r;

    for (int i = 0; i < _nbPoints; i++)
    {
        do {
            //x = (float)im3.width() * drand48();
            //y = (float)im3.height() * drand48();

            //poisson on disk
            //float randR = double(rng())/(rng.max());
            //float theta = 2.0 * M_PI * double(rng())/(rng.max());
            //x = randR*cos(theta);
            //y = randR*sin(theta);

            //poisson on square
            x = double(rng())/(rng.max());
            y = double(rng())/(rng.max());



            tooClose = false;
            for (int j = 0; j < i; j++) {
                xl = x - xs[j];
                yl = y - ys[j];
                if (xl*xl + yl*yl < r*r) { tooClose = true; break; }
            }
        } while (tooClose == true);


        //xs[i] = (x+1.f)/2.f;
        //ys[i] = (y+1.f)/2.f;

        /*
         * jitter
        x2 = x + offsetX;
        y2 = y + offsetY;

        bool reprojected = false;

        double r2 = sqrt(x2*x2+y2*y2);
        //point outside of the disk
        if(r2>1.0)
        {
            double phi = acos(x2/r2);
            r2 = 2.0-r2;
            phi = phi+M_PI;

            x2 = r2*cos(phi);
            y2 = r2*sin(phi);
            reprojected = true;
        }
        */
/*
        //Draw initial poisson distribution
        posX = margin+(x+1.0)/2.0*(_im.width()-2*margin);
        posY = margin+(y+1.0)/2.0*(_im.height()-2*margin);

        drawPoint(_im,posX,posY,pointColor);
        drawPoint(_im2,posX,posY,pointColor);

        drawPoint(_imDimX,posX,5,qRgb(0,0,0));
        drawPoint(_imDimY,posY,5,qRgb(0,0,0));

        //Draw jitterred poisson distribution
        posX = margin+(x2+1.0)/2.0*(_im.width()-2*margin);
        posY = margin+(y2+1.0)/2.0*(_im.height()-2*margin);

        if(reprojected)
        {
            drawPoint(_im,posX,posY,qRgb(255,0,255));
        }
        else
        {
            drawPoint(_im,posX,posY,qRgb(0,0,255));
        }
        */

        //Store min and max values of initial poissson distribution
        maxX = max(maxX,xs[i]);
        minX = min(minX,xs[i]);
        maxY = max(maxY,ys[i]);
        minY = min(minY,ys[i]);

        (*_sampling)[i] = std::make_pair(xs[i],ys[i]);

        //ui->_progressBar->setValue(static_cast<float>(i+1)/static_cast<float>(nbPoints));
        emit progressValue(int(100.f*float(i+1)/float(_nbPoints)));
    }




    emit progressValue(100);

    delete [] xs;
    delete [] ys;

/*
    //Draw circles around distributions
    QPainter painter;
    painter.begin(&_im);
    painter.setPen(Qt::black);
    painter.drawEllipse(margin,margin,_im.width()-2*margin,_im.height()-2*margin);
    painter.end();

    painter.begin(&_im2);
    painter.setPen(Qt::black);
    painter.drawEllipse(margin,margin,_im2.width()-2*margin,_im.height()-2*margin);

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    ui->_lImage2->setPixmap(QPixmap::fromImage(_im2));
    ui->_lImageDimX->setPixmap(QPixmap::fromImage(_imDimX));
    ui->_lImageDimY->setPixmap(QPixmap::fromImage(_imDimY));

    ui->_progressBar->setValue(100);

    */
    emit finished();
}
