#include "sphericalcoordinates.h"

SphericalCoordinates::SphericalCoordinates(float theta, float phi):_theta(theta), _phi(phi){  }

float SphericalCoordinates::getTheta() const{
    return _theta;
}

float SphericalCoordinates::getPhi() const{
    return _phi;
}
