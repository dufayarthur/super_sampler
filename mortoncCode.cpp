#include "mortonCode.h"

#include <algorithm>
#include <math.h>
#include <iostream>
#include <assert.h>

MortonCode::MortonCode(std::vector<Point> & points){
    std::vector<Point>::iterator it;
    std::map<int, std::vector<Point> >::iterator it_map;

    for(it=points.begin(); it != points.end(); ++it){
        int valueCode;
        mortonEntrelace(it->getX(), it->getY(), valueCode);

        it_map = _correspondingAngleWithPoints.find(valueCode);

        if(it_map != _correspondingAngleWithPoints.end()){
           it_map->second.push_back(*it);
        }
        else{
            std::vector<Point> pts;
            pts.push_back(*it);

            _correspondingAngleWithPoints.insert(std::pair<int, std::vector<Point> >(valueCode, pts));
        }
    }
}

void MortonCode::codeForCoordinates2D(std::vector<Coordinates2D> & coords){
    std::vector<Point>::iterator it;
    std::map<int, std::vector<Point> >::iterator it_map;

    for(it_map=_correspondingAngleWithPoints.begin(); it_map != _correspondingAngleWithPoints.end(); ++it_map){
        std::vector<Point> pointsAngle = it_map->second;

        for(it=pointsAngle.begin(); it != pointsAngle.end(); ++it){
            coords.push_back(Coordinates2D(it->getX(), it->getY()));
         }
    }
}

void MortonCode::codeForSphericalCoordinates(std::vector<SphericalCoordinates> & coords){
    std::vector<Point>::iterator it;
    std::map<int, std::vector<Point> >::iterator it_map;

    for(it_map=_correspondingAngleWithPoints.begin(); it_map != _correspondingAngleWithPoints.end(); ++it_map){
        std::vector<Point> pointsAngle = it_map->second;

        for(it=pointsAngle.begin(); it != pointsAngle.end(); ++it){
            //std::cout<<it->getX() << " " << it->getY()<< " " << it->getZ()<< " " << it->getTheta()/2.0*M_PI<< " "<< (it->getPhi()*(2.0 * M_PI))-M_PI<<std::endl;
            coords.push_back(SphericalCoordinates(it->getTheta(), it->getPhi()));
         }
    }
}

void MortonCode::mortonEntrelace(float x, float y, int & bits){
    bits = 0;

    int X = ceil(((x + 1.0)/2.0) * 255.0);
    int Y = ceil(((y + 1.0)/2.0) * 255.0);

    for(int i = 0; i < 8 ; i++){
        int bitY = (int)(Y & 0x01);
        Y = Y >> 1;

        int bitX = (int)(X & 0x01);
        X = X >> 1;

        bits = (int)(bits >> 1) + (int)(bitX << 15);
        bits = (int)(bits >> 1) + (int)(bitY << 15);
    }
}
