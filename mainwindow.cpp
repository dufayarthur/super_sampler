#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "poissonsampling.h"
#include <QApplication>
#include <QImage>
#include <QLabel>
#include <QPainter>
#include <QGenericMatrix>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <cmath>
#include <QPointF>
#include <QColorDialog>
#include <QFileDialog>
#include <QThread>
#include <QMouseEvent>

#include <QToolTip>

#include "point.h"
#include "mortonCode.h"
#include "sobol.h"
#include "threaddartthrowing.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->_pbGenerate, SIGNAL(clicked()), this, SLOT(generateSampling()));
    QObject::connect(ui->_pbSaveImage, SIGNAL(clicked()), this, SLOT(saveImage()));
    QObject::connect(ui->_sbNbPoints, SIGNAL(valueChanged(int)), this, SLOT(updateMaxR(int)));
    QObject::connect(ui->_pbSave, SIGNAL(clicked()), this, SLOT(saveSampling()));
    QObject::connect(ui->_pbOrder, SIGNAL(clicked()), this, SLOT(orderSampling()));
    QObject::connect(ui->_pbProjectOnDisk, SIGNAL(clicked()), this, SLOT(projectOnDisk()));
    QObject::connect(ui->_pbApplyJitter, SIGNAL(clicked()), this, SLOT(applyJitter()));
    QObject::connect(ui->_pbApplyRotation, SIGNAL(clicked()), this, SLOT(applyRotation()));
    QObject::connect(ui->_pbPointColor, SIGNAL(clicked()), this, SLOT(showPointColorPicker()));
    QObject::connect(ui->_pbPointColor2, SIGNAL(clicked()), this, SLOT(showPointColorPicker2()));
    QObject::connect(ui->_pbApplyJitterAndProject, SIGNAL(clicked()), this, SLOT(applyJitterAndProject()));
    QObject::connect(ui->_pbDrawPath, SIGNAL(clicked()), this, SLOT(drawPathBetweenPoints()));
    QObject::connect(ui->_pbStarDiscrepancy, SIGNAL(clicked()), this, SLOT(computeStarDiscrepancy()));
    QObject::connect(ui->_pbCummStarD, SIGNAL(clicked()), this, SLOT(computeCummulativeStarDiscrepancy()));
    QObject::connect(ui->_pbSaveCummStarD, SIGNAL(clicked()), this, SLOT(saveCummulativeStarDiscrepancy()));

    QObject::connect(ui->_cbForceJitterValues, SIGNAL(stateChanged(int)), this, SLOT(enableSBJitter(int)));

    //Redraw event
    QObject::connect(ui->_sbImageSize, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbPointSize, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbImageMargin, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColorR, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColorG, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColorB, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColor2R, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColor2G, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));
    QObject::connect(ui->_sbColor2B, SIGNAL(valueChanged(int)), this, SLOT(redraw(int)));


    _applicationSettings = ApplicationSettings::Instance();
    _applicationSettings->setCurrentDirectory(QDir::homePath().toStdString());

    qApp->installEventFilter(this);
    //qApp->installEventFilter(ui->_lImage);

    _lastSamplingGenerated = "none";

    //Background image color
    //_imFillColor = QColor(33,33,33);
    _imFillColor = QColor(255,255,255);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QPoint MainWindow::samplingCoordToImageCoord(float x, float y, int margin, int width, int height)
{
    //convert [0,1]x[0,1] to image coords and reverse y coord
    return QPoint(margin + x*(width-2*margin), height - 1 - (margin + y*(height-2*margin)));
}

void MainWindow::drawSamplingOnSquare(const std::vector<std::pair<float,float>>& sampling, bool useSencondaryColor)
{
    unsigned int M = ui->_sbImageSize->value();
    int margin = ui->_sbImageMargin->value();
    QPoint pos;

    bool drawSquares = ui->_rbSquare->isChecked();

    if( ! ui->_cbKeepLastSampling->isChecked() )
    {
        _im = QImage(M,M,QImage::Format_RGB888);
        _im.fill(_imFillColor);
    }

    _imDimX = QImage(M,10,QImage::Format_RGB888);
    _imDimX.fill(QColor(255,255,255));

    _imDimY = QImage(M,10,QImage::Format_RGB888);
    _imDimY.fill(QColor(255,255,255));

    QRgb pointColor;
    if(useSencondaryColor)
    {
        pointColor = qRgb(ui->_sbColor2R->value(),ui->_sbColor2G->value(),ui->_sbColor2B->value());
    }

    else
    {
        pointColor = qRgb(ui->_sbColorR->value(),ui->_sbColorG->value(),ui->_sbColorB->value());
    }

    QPainter painter;
    painter.begin(&_im);
    QPen pen;
    if(drawSquares)
    {
        pen.setWidth(ui->_sbPointSize->value());
        pen.setColor(pointColor);
        painter.setPen(pen);
    }
    else
    {
        painter.setPen(pointColor);
        painter.setBrush(QBrush(QColor(pointColor)));
    }

    //Basic painter for images dimX and dimY
    QPainter painterDimX;
    painterDimX.begin(&_imDimX);
    QPen penDim;
    penDim.setColor(QColor(0,0,0));
    penDim.setWidth(2);
    painterDimX.setPen(penDim);

    QPainter painterDimY;
    painterDimY.begin(&_imDimY);
    painterDimY.setPen(penDim);

    for (uint i = 0; i < sampling.size(); i++)
    {
        //posX = sampling[i].first * (_im.width() - 2 * margin) + margin;
        //posY = sampling[i].second * (_im.height() - 2 * margin) + margin;

        pos = samplingCoordToImageCoord(sampling[i].first, sampling[i].second, margin, _im.width(), _im.height());

        //draw on image
        if(drawSquares)
            painter.drawPoint(pos);
        else
            painter.drawEllipse( pos, ui->_sbPointSize->value(), ui->_sbPointSize->value() );

        painterDimX.drawPoint(pos.x(),5);
        //reverse posY for image dim Y
        painterDimY.drawPoint(_imDimY.width() - 1 - pos.y(),5);
    }

    painter.end();
    painterDimX.end();
    painterDimY.end();

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    ui->_lImageDimX->setPixmap(QPixmap::fromImage(_imDimX));
    ui->_lImageDimY->setPixmap(QPixmap::fromImage(_imDimY));
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
  if (event->type() == QEvent::MouseMove)
  {
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
    int mouseX = mouseEvent->windowPos().x();//mouseEvent->pos().x();
    int mouseY = mouseEvent->windowPos().y();//mouseEvent->pos().y();
    //statusBar()->showMessage(QString("Mouse move (%1,%2,%3,%4,%5)").arg(mouseX).arg(mouseY).arg(mouseIsOverImage(mouseX,mouseY)).arg(ui->_lImage->y()).arg(ui->_lImage->underMouse()));
    //statusBar()->showMessage(QString("Mouse move (%1,%2,%3,%4)").arg(ui->_lImage->size().width()).arg(ui->_lImage->size().height()).arg(ui->_lImage->pos().x()).arg(ui->_lImage->pos().y()));

    int mousePosInImageX = mouseX - ui->_lImage->x();
    int mousePosInImageY = mouseY - ui->_lImage->y();

    //mouse is over image sampling
    if(ui->_lImage->underMouse())
    {
        showTooltipSample(mousePosInImageX,mousePosInImageY,event);
        statusBar()->showMessage(QString("Mouse move (%1,%2,%3,%4)").arg(mousePosInImageX).arg(mousePosInImageY).arg(mouseX).arg(mouseY));
    }
  }
  return false;
}

bool MainWindow::showTooltipSample(int mouseX, int mouseY, QEvent *event)
{
    int offsetZone = 5;

    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

    int margin = ui->_sbImageMargin->value();
    auto tempSampling = _sampler.getSampling();
    uint numSample = 0;
    bool found = false;
    QPoint pos;

    for(uint i=0; i < tempSampling.size() && !found; i++)
    {
        pos = samplingCoordToImageCoord(tempSampling[i].first, tempSampling[i].second, margin, _im.width(), _im.height());

        if( mouseX >= pos.x() - offsetZone &&
            mouseX <= pos.x() + offsetZone &&
            mouseY >= pos.y() - offsetZone &&
            mouseY <= pos.y() + offsetZone )
        {
            numSample = i;
            found = true;
        }
    }

    if(found)
    {
        //QToolTip::hideText();
        QToolTip::showText(mouseEvent->globalPos(), "sample num " + QString::number(numSample)
                           + " "
                           + QString::number(_sampler.getSample(numSample).first)
                           + " "
                           + QString::number(_sampler.getSample(numSample).second)
                           +" "
                           , this, rect());
        return true;
    }

    return false;
}

void MainWindow::generateSampling()
{
    uint nbPoints = ui->_sbNbPoints->value();

    ui->_lImage->clear();
    ui->_lImage2->clear();

    QString qsNum;

    if(ui->_rbHalton->isChecked())
    {
        _sampler.generateHaltonSampling(nbPoints, ui->_sbSequenceDimX->value(), ui->_sbSequenceDimY->value(), ui->_sbOffsetIndexStartSequence->value());

        _lastSamplingGenerated = "halton_";

        qsNum.setNum(ui->_sbSequenceDimX->value());
        _lastSamplingGenerated += qsNum+"_";

        qsNum.setNum(ui->_sbSequenceDimY->value());
        _lastSamplingGenerated += qsNum+"_";

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated += qsNum;
    }
    else if(ui->_rbHammersley->isChecked())
    {
        _sampler.generateHammersleySampling(nbPoints, ui->_sbOffsetIndexStartSequence->value());

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "hammersley_"+qsNum;
    }
    else if(ui->_rbWang->isChecked())
    {
        _sampler.generateWangSampling(nbPoints);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "wang_"+qsNum;
    }
    else if(ui->_rbShaderToy->isChecked())
    {
        _sampler.generateShaderToySampling(nbPoints);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "shader_toy_"+qsNum;
    }
    else if(ui->_rbJittered->isChecked())
    {
        _sampler.generateJitteredSampling(nbPoints);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "jittered_"+qsNum;
    }
    else if(ui->_rbPoisson->isChecked())
    {
        double r = ui->_dsbR->value();
        _sampler.generatePoissonSampling(nbPoints, r);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "poisson_"+qsNum;
    }
    else if(ui->_rbPoissonJittered->isChecked())
    {
        double r = ui->_dsbR->value();
        _sampler.generatePoissonSamplingJittered(nbPoints, r);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "poisson_jittered_"+qsNum;
    }
    else if(ui->_rbRegular->isChecked())
    {
        _sampler.generateRegularSampling(nbPoints);

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "regular_"+qsNum;
    }
    else if(ui->_rbSobol->isChecked())
    {
        _sampler.generateSobolSampling(nbPoints, ui->_sbSequenceDimY->value(), ui->_sbOffsetIndexStartSequence->value());

        qsNum.setNum(_sampler.getSampling().size());
        _lastSamplingGenerated = "sobol_"+qsNum;
    }
    else
    {
        //TODO add poisson rotation in ui
        //double r = ui->_dsbR->value();
        //_sampler.generatePoissonSamplingRotation(nbPoints, r);
    }
    drawSamplingOnSquare(_sampler.getSampling());
}

void MainWindow::saveImage()
{
    QString qs = _lastSamplingGenerated + ".png";
    QString qsX = _lastSamplingGenerated + "_dimX.png";
    QString qsY = qs + "_dimY.png";

    _im.save(qs);
    _imDimX.save(qsX);
    _imDimY.save(qsY);

    qs = "Image save to " + QDir::currentPath() + qs;
    //ui->_lInfo->setText(qs);
    statusBar()->showMessage(qs);
}


void MainWindow::updateMaxR(int nbPoints)
{
    double maxR = 1.0/sqrt(nbPoints);
    ui->_lRecommendedJitter->setNum(maxR);
}

void MainWindow::saveSampling()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               QString::fromStdString(_applicationSettings->currentDirectory()),
                               tr("*.csv"));
    if(fileName.size()==0)
        return;

    QFile file(fileName);
    _applicationSettings->setCurrentDirectory(QFileInfo(fileName).dir().absolutePath().toStdString());

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
      return;

    QTextStream out(&file);

    auto tempSampling = _sampler.getSampling();

    for(int i=0;i<tempSampling.size();i++){
        out<< tempSampling[i].first<< ", " << tempSampling[i].second <<",\n";
    }

    file.close();

    QString qs = "Sampling save to " + fileName;
    statusBar()->showMessage(qs);
}

void MainWindow::orderSampling()
{
    _sampler.orderSampling();
    statusBar()->showMessage("Order sampling Done");
}

void MainWindow::projectOnDisk()
{    
    _sampler.projectSamplingOnDisk();

    //draw sampling
    drawSamplingOnSquare(_sampler.getSampling());

    //draw surrounding disk
    int margin = ui->_sbImageMargin->value();
    QPainter painter;
    painter.begin(&_im);
    QPen pen;
    pen.setWidth(3);
    pen.setColor(QColor(ui->_sbColorR->value(),ui->_sbColorG->value(),ui->_sbColorB->value()));

    painter.setPen(pen);
    painter.setPen(Qt::black);
    painter.drawEllipse(margin,margin,_im.width()-2*margin,_im.height()-2*margin);
    painter.end();

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
    ui->_lImageDimX->setPixmap(QPixmap::fromImage(_imDimX));
    ui->_lImageDimY->setPixmap(QPixmap::fromImage(_imDimY));
}

void MainWindow::applyJitter()
{
    //uint nbJitter = 150;
    //for(uint i=0;i<nbJitter;i++)
    {
        if(ui->_cbForceJitterValues->isChecked())
        {
            double jitterX = ui->_sbJitterX->value();
            double jitterY = ui->_sbJitterY->value();
            auto tempSampling = _sampler.applyJitter(jitterX,jitterY);
            drawSamplingOnSquare(tempSampling,true);
        }
        else
        {
            auto tempSampling = _sampler.applyJitter(ui->_dsbR->value());
            //
            drawSamplingOnSquare(tempSampling,true);
        }

        const std::pair<double,double>& jitter = _sampler.getLastJitter();
        //ui->_lInfo->setText("Jitter was: "+QString::number(jitter.first)+" "+QString::number(jitter.second));
        statusBar()->showMessage("Jitter was: "+QString::number(jitter.first)+" "+QString::number(jitter.second));
    }
}

void MainWindow::applyRotation()
{
    auto tempSampling = _sampler.applyRotation();
    //
    drawSamplingOnSquare(tempSampling,true);

}

void MainWindow::applyJitterAndProject()
{
    /*
     * TODO move this to sampler
     *
    float jitterX;
    float jitterY;

    if(ui->_cbForceJitterValues->isChecked())
    {
        jitterX = ui->_sbJitterX->value();
        jitterY = ui->_sbJitterY->value();
    }
    else
    {
        jitterX = (2.0*drand48()-1.0) * ui->_dsbR->value();
        jitterY = (2.0*drand48()-1.0) * ui->_dsbR->value();
        ui->_lInfo->setText("Jitter was: "+QString::number(jitterX)+" "+QString::number(jitterY));
    }


    std::vector<std::pair<float,float>> tempSampling;
    tempSampling.resize(_sampling.size());

    for (int i = 0; i < tempSampling.size(); i++)
    {

        float x = _sampling[i].first + jitterX;
        float y = _sampling[i].second + jitterY;

        if(x<0.0)
            x += 1.0;
        else if(x>=1.0)
            x -= 1.0;

        if(y<0.0)
            y += 1.0;
        else if(y>=1.0)
            y -= 1.0;


        tempSampling[i] = std::make_pair(x,y);
    }

    bool keepState = ui->_cbKeepLastSampling->isChecked();
    QColor lastColor(ui->_sbColorR->value(),ui->_sbColorG->value(),ui->_sbColorB->value());
    QColor color2(ui->_sbColor2R->value(),ui->_sbColor2G->value(),ui->_sbColor2B->value());

    ui->_cbKeepLastSampling->setChecked(false);
    projectOnDisk();

    ui->_sbColorR->setValue(color2.red());
    ui->_sbColorG->setValue(color2.green());
    ui->_sbColorB->setValue(color2.blue());

    ui->_cbKeepLastSampling->setChecked(true);
    _sampling = tempSampling;
    projectOnDisk();
    ui->_cbKeepLastSampling->setChecked(keepState);

    ui->_sbColorR->setValue(lastColor.red());
    ui->_sbColorG->setValue(lastColor.green());
    ui->_sbColorB->setValue(lastColor.blue());
    */
}

void MainWindow::showPointColorPicker()
{
    QColor pointColor(ui->_sbColorR->value(),ui->_sbColorG->value(),ui->_sbColorB->value());

    pointColor = QColorDialog::getColor(pointColor, this, "Choose point color");

    ui->_sbColorR->setValue(pointColor.red());
    ui->_sbColorG->setValue(pointColor.green());
    ui->_sbColorB->setValue(pointColor.blue());
}

void MainWindow::showPointColorPicker2()
{
    QColor pointColor(ui->_sbColor2R->value(),ui->_sbColor2G->value(),ui->_sbColor2B->value());

    pointColor = QColorDialog::getColor(pointColor, this, "Choose point color");

    ui->_sbColor2R->setValue(pointColor.red());
    ui->_sbColor2G->setValue(pointColor.green());
    ui->_sbColor2B->setValue(pointColor.blue());
}

void MainWindow::enableSBJitter(int state)
{
    if(state==Qt::Checked)
    {
        ui->_sbJitterX->setEnabled(true);
        ui->_sbJitterY->setEnabled(true);
    }
    else
    {
        ui->_sbJitterX->setEnabled(false);
        ui->_sbJitterY->setEnabled(false);
    }
}

void MainWindow::setProgressValue(int val)
{
    ui->_progressBar->setValue(val);
}

void MainWindow::redraw(int val)
{
    drawSamplingOnSquare(_sampler.getSampling());
}

void MainWindow::drawPathBetweenPoints()
{
    QPainter painter;
    painter.begin(&_im);
    QPen pen;
    pen.setColor(QColor(0,0,0));

    auto sampling = _sampler.getSampling();

    int margin = ui->_sbImageMargin->value();

    QPoint pos,posNext;

    for (uint i = 0; i < sampling.size() - 1; i++)
    {
        //posX = sampling[i].first * (_im.width() - 2 * margin) + margin;
        //posY = sampling[i].second * (_im.height() - 2 * margin) + margin;

        pos = samplingCoordToImageCoord(sampling[i].first, sampling[i].second, margin, _im.width(), _im.height());
        posNext = samplingCoordToImageCoord(sampling[i+1].first, sampling[i+1].second, margin, _im.width(), _im.height());

        painter.drawLine(pos,posNext);
    }

    painter.end();

    ui->_lImage->setPixmap(QPixmap::fromImage(_im));
}

void MainWindow::computeStarDiscrepancy()
{
    const std::pair<double,double>& discrepancy = _sampler.computeStarDiscrepancy();
    statusBar()->showMessage("Star discrepancy: "+QString::number(discrepancy.first)+" "+QString::number(discrepancy.second));

}

void MainWindow::computeCummulativeStarDiscrepancy()
{
    _sampler.computeCummulativeStarDiscrepancy();
    statusBar()->showMessage("Cummulative Star discrepancy Computed");
}

void MainWindow::saveCummulativeStarDiscrepancy()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               QString::fromStdString(_applicationSettings->currentDirectory()),
                               tr("*.csv"));
    if(fileName.size()==0)
        return;

    QFile file(fileName);
    _applicationSettings->setCurrentDirectory(QFileInfo(fileName).dir().absolutePath().toStdString());

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
      return;

    QTextStream out(&file);

    auto tempSampling = _sampler.getCummulativeStarDiscrepancy();

    for(int i=0;i<tempSampling.size();i++){
        out<< tempSampling[i].first<< ", " << tempSampling[i].second <<",\n";
    }

    file.close();

    QString qs = "Cummulative Star Discrepancy save to " + fileName;
    statusBar()->showMessage(qs);
}

