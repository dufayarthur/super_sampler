#include "coordinates2d.h"

Coordinates2D::Coordinates2D(float x, float y):_x(x), _y(y){  }

float Coordinates2D::getX() const{
    return _x;
}

float Coordinates2D::getY() const{
    return _y;
}
