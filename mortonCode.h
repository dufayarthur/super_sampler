#ifndef MORTONCODE_H
#define MORTONCODE_H

#include <map>
#include <vector>

#include "point.h"
#include "coordinates2d.h"
#include "sphericalcoordinates.h"

class MortonCode{
    public:
        MortonCode(std::vector<Point> & points);

        void codeForCoordinates2D(std::vector<Coordinates2D> & coords);
        void codeForSphericalCoordinates(std::vector<SphericalCoordinates> & coords);

    private:
        void mortonEntrelace(float x, float y, int & bits);

    private:
        std::map<int, std::vector<Point> >  _correspondingAngleWithPoints;
};
#endif // MORTONCODE_H
