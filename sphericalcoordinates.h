#ifndef SPHERICALCOORDINATES_H
#define SPHERICALCOORDINATES_H

class SphericalCoordinates{
    public:
        SphericalCoordinates(float theta = 0.0, float phi = 0.0);

        float getTheta() const;
        float getPhi() const;

    private:
        float _theta, _phi;
};

#endif // SPHERICALCOORDINATES_H
