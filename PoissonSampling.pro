#-------------------------------------------------
#
# Project created by QtCreator 2014-09-24T14:44:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PoissonSampling
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    poissonsampling.cpp \
    applicationsettings.cpp \
    coordinates2d.cpp \
    mortoncCode.cpp \
    point.cpp \
    sphericalcoordinates.cpp \
    stratifiedSampling.cpp \
    sobol.cpp \
    threaddartthrowing.cpp \
    sampler.cpp \
    star_discrepancy.cpp

HEADERS  += mainwindow.h \
    poissonsampling.h \
    applicationsettings.h \
    coordinates2d.h \
    mortonCode.h \
    point.h \
    sphericalcoordinates.h \
    stratifiedSampling.h \
    sobol.h \
    threaddartthrowing.h \
    sampler.h \
    star_discrepancy.h

FORMS    += mainwindow.ui

RESOURCES += \
    ressource.qrc
