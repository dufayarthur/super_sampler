#ifndef THREADDARTTHROWING_H
#define THREADDARTTHROWING_H

#include <QObject>
#include <vector>

class ThreadDartThrowing : public QObject
{
    Q_OBJECT
public:
    ThreadDartThrowing();
    ~ThreadDartThrowing();
    void setData(std::vector<std::pair<float,float>>* sampling);
    void setParameters(unsigned int nbPoints, float radius);

signals:
    void finished();
    void error(QString err);
    void progressValue(int val);

public slots:
    void process();

private:
    std::vector<std::pair<float,float>>* _sampling;
    unsigned int _nbPoints;
    float _radius;
};

#endif // THREADDARTTHROWING_H
