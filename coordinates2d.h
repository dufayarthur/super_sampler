#ifndef COORDINATES_H
#define COORDINATES_H

class Coordinates2D{
    public:
        Coordinates2D(float x = 0.0, float y = 0.0);

        float getX() const;
        float getY() const;

    private:
        float _x, _y;
};
#endif // COORDINATES_H
