#ifndef POINT_H
#define POINT_H

#include "coordinates2d.h"
#include "sphericalcoordinates.h"

class Point{
    public:
        Point(float x=0, float y=0);

        float getX() const;
        float getY() const;
        float getZ() const;
        float getR() const;

        float getTheta() const;
        float getPhi() const;

        Coordinates2D getXY() const;
private:
        float computeTheta();
        float computePhi();

    private:
        Coordinates2D _xy;
        SphericalCoordinates _thetaPhi;

        float _z;
        float _r;


};
#endif // POINT_H
