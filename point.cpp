#include "point.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>

#include <iostream>

Point::Point(float x, float y){
    _xy = Coordinates2D(x, y);
    _r = sqrtf(getX() * getX() + getY() * getY());
    _z = sqrtf(1.f - _r * _r);
    _r = sqrtf(getX() * getX() + getY() * getY() + getZ() * getZ());

    float theta = computeTheta();
    float phi = computePhi();

    _thetaPhi = SphericalCoordinates(theta, phi);

    //std::cout<< getX() << " " << getY() << " " << getZ() << " " << getR()<< " " << atan2(_xy.getY(), _xy.getX())<< " " << acos(_z)<<std::endl;
}

float Point::getX() const{
   return _xy.getX();
}

float Point::getY() const{
    return _xy.getY();
}

float Point::getZ() const{
    return _z;
}

float Point::getR() const{
    return _r;
}

float Point::getTheta() const {
    return _thetaPhi.getTheta();
}

float Point::getPhi() const {
    return _thetaPhi.getPhi();
}


float Point::computeTheta(){
   float theta = acos(_z/_r);// between [-PI/2 Pi/2]

   // std::cout<<theta<<std::endl;
   return theta * 2.0 / M_PI ;//clamp between [0.0 1.0]
}

float Point::computePhi(){
    float phi = atan2(_xy.getY(), _xy.getX());//tan(phi) = r / z // between [ 0 2PI ]

    float twoPi = 2.0 * M_PI;

    //std::cout<<phi<< " " <<(phi + M_PI) / twoPi<<std::endl;
    // std::cout<<acos(_z / _r)<<std::endl;
    return (phi + M_PI) / twoPi;//between [0.0 1.0]
}

Coordinates2D Point::getXY() const{
    return _xy;
}
