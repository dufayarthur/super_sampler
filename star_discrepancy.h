#ifndef STAR_DISCREPANCY_H
#define STAR_DISCREPANCY_H

#include <utility>

typedef std::pair<float,float> Sample;

void getStarDiscrepancy(const std::vector<Sample>& sampling, unsigned int nbPoints, std::pair<double,double>& starDiscrepancy, double epsilon);

typedef struct som sommet;

struct som
{
  int id;
  sommet *pt;
};

class StarDiscrepancyComputer
{
public:

    int s;
    int smin;
    int taille;
    int num;
    int den;
    int num2;
    int subtotala = 0;
    int subtotalb = 0;
    int *lexsizealpha;
    int *maxsizealpha;
    int *lexsizebeta;
    int *maxsizebeta;
    double p;
    double borne = 0.0;
    double borneinf = 0.0;
    double **points;
    sommet *superarbre;
    sommet **lexalpha;
    sommet **lexbeta;
    FILE *fichier;

    void decomposition ( double *alpha, double *beta, int min, double value );
    int explore ( sommet *liste, double *pave, int dim );
    int fastexplore ( double *pave, int range, int *maxsize, int *lexsize,
      sommet **lex, int *subtotal );
    void fileformat ( void );
    void freetree ( sommet *noeud );
    void initlex ( void );
    void freeLexMemory();
    void insertlex ( sommet *noeud, int range, int *maxsize, int *lexsize,
      sommet **lex );
    double lowbound ( int npoints, double volume, double *pave );
    void memory ( void );
    void quicksort ( sommet *liste, int dim, int l, int r );
    void readfile ( char *filename );
    sommet *subtree ( sommet *liste, int min, int next, int dim );
    void supertree ( void );
    void traiter ( double *outputalpha, double *outputbeta, int range );
    void usage ( char *nom );
};


#endif // STAR_DISCREPANCY_H
