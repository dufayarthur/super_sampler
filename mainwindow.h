#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>

#include "applicationsettings.h"
#include "sampler.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QPoint samplingCoordToImageCoord(float x, float y, int margin, int width, int height);
    void drawSamplingOnSquare(const std::vector<std::pair<float, float> > &sampling, bool useSencondaryColor=false);
    bool eventFilter(QObject *obj, QEvent *event);
    bool showTooltipSample(int mouseX, int mouseY, QEvent *event);
    int findSampleUnderMouse(int mouseX, int mouseY);



public slots:
    void generateSampling();
    void updateMaxR(int nbPoints);
    void saveImage();
    void saveSampling();
    void orderSampling();
    void projectOnDisk();
    void applyJitter();
    void applyRotation();
    void applyJitterAndProject();
    void showPointColorPicker();
    void showPointColorPicker2();
    void enableSBJitter(int state);
    void setProgressValue(int val);
    void redraw(int);
    void drawPathBetweenPoints();
    void computeStarDiscrepancy();

    void computeCummulativeStarDiscrepancy();
    void saveCummulativeStarDiscrepancy();


private:
    Ui::MainWindow *ui;
    QImage _im;
    QImage _im2;
    QImage _imDimX;
    QImage _imDimY;
    QColor _imFillColor;
    //QToolTip _tooltip;
    //save name of last sampling for auto naming of saved sampling images
    QString _lastSamplingGenerated;

    ApplicationSettings* _applicationSettings;

    Sampler _sampler;
};

#endif // MAINWINDOW_H
