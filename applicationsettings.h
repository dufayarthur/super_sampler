#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H

#include <string>

class ApplicationSettings
{
public:
    static ApplicationSettings* Instance();
    static void kill();
    std::string currentDirectory() const;
    void setCurrentDirectory(const std::string &currentDirectory);

private:
    ApplicationSettings();
    ~ApplicationSettings();

    static ApplicationSettings* _instance;

    std::string _currentDirectory;


};

#endif // APPLICATIONSETTINGS_H
