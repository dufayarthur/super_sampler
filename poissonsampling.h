#ifndef POISSONSAMPLING_H
#define POISSONSAMPLING_H

#include <QPointF>

class PoissonSampling
{
public:
    PoissonSampling(int nbPoints);
    PoissonSampling();
    ~PoissonSampling();
    QPointF getSample(unsigned int num);
    float getSampleX(unsigned int num);
    float getSampleY(unsigned int num);
private:
    float* _sampling;
};

#endif // POISSONSAMPLING_H
